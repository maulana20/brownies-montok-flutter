import 'package:flutter/material.dart';
import 'package:ketixclone/customWidget/widget.dart';
import 'package:carousel_pro/carousel_pro.dart';

class HomePageOffline extends StatefulWidget {
  _HomePageOfflineState createState() => _HomePageOfflineState();
}

class _HomePageOfflineState extends State<HomePageOffline> {
  @override
  Widget build(BuildContext context) {
    var imageSlide = new SizedBox(
        height: 150.0,
        width: double.infinity,
        child: new Carousel(
          images: [
            new AssetImage("./img/banner1.png"),
            new AssetImage("./img/banner2.png"),
            new AssetImage("./img/banner3.png"),
            new AssetImage("./img/banner4.png"),
          ],
          showIndicator: false,
          boxFit: BoxFit.fill,
          autoplayDuration: Duration(milliseconds: 5000),
          indicatorBgPadding: 1.0,
        ));
    return ListView(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      children: <Widget>[
        imageSlide,
        GridView.count(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          crossAxisCount: 3,
          childAspectRatio: 1.3,
          children: <Widget>[
            ListItem(
              img: "./img/product.png",
              text: "Product",
              onPressed: () {},
            ),
            ListItem(
              img: "./img/cart.png",
              text: "Cart",
              onPressed: () {},
            ),
            ListItem(
                img: "./img/transactions.png",
                text: "Transactions",
                onPressed: () {}),
            ListItem(
              img: "./img/help.png",
              text: "Information",
              onPressed: () {},
            ),
            ListItem(
              img: "./img/specialdeals.png",
              text: "Promo",
              onPressed: () {},
            ),
            ListItem(
              img: "./img/about_us.png",
              text: "About Us",
              onPressed: () {},
            ),
          ],
        ),
      ],
    );
  }
}
