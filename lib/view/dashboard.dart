import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:ketixclone/api.dart';
import 'package:ketixclone/view/admin/adminReservation.dart';
import 'package:ketixclone/view/everything/home.dart';
import 'package:ketixclone/view/login/login.dart';
import 'package:ketixclone/view/offline/offlineGrid.dart';
import 'package:ketixclone/view/offline/offlineNews.dart';
import 'package:ketixclone/view/product/news.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class DashboardApp extends StatefulWidget {
  final VoidCallback keluar;
  final bool _status;
  DashboardApp(this.keluar, this._status);
  _DashboardAppState createState() => _DashboardAppState();
}

class _DashboardAppState extends State<DashboardApp> {
  String username, level, idCustomer;
  getPreference() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      username = preferences.getString('username');
      level = preferences.getString('level');
      idCustomer = preferences.getString('idCustomer');
    });
    _checkKoneksi();
  }

  _submitHapus() async {
    final response = await http.post(
        ApiUrl.baseUrl + ApiUrl.hapusSemuaTransaksi,
        body: {"username": username, "idCustomer": idCustomer});
    final data = jsonDecode(response.body);
    int value = data['value'];
    if (value == 1) {
      setState(() {
        Text('Berhasil di hapus');
      });
    }
  }

  _checkKoneksi() {
    if (widget._status) {
      _statusOffline();
    }
  }

  @override
  void initState() {
    super.initState();
    getPreference();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        centerTitle: true,
        title: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Image.asset('./img/brownies_logo.png'),
        ),
        actions: <Widget>[
          GestureDetector(
            onTap: _exitPopUp,
            child: Padding(
              padding: const EdgeInsets.all(14.0),
              child: Image.asset(
                './img/keluar.png',
                height: 30.0,
                width: 30.0,
              ),
            ),
          )
        ],
      ),
      body: Container(
        child: MediaQuery.removePadding(
          removeTop: true,
          context: context,
          child: ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: 2,
            itemBuilder: (context, page) {
              if (page == 0) {
                return widget._status == true ? HomePageOffline() : HomePage();
              }
              if (page == 1) {
                return ListView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: <Widget>[
                    Padding(
                      padding:
                          EdgeInsets.only(left: 8.0, top: 16.0, bottom: 5.0),
                      child: Text(
                        "Hot's New Brownies",
                        style: TextStyle(
                            color: Colors.brown,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    widget._status == true ? NewsOffline() : NewsUpdate()
                  ],
                );
              }
            },
          ),
        ),
      ),
      floatingActionButton: widget._status == true
          ? SizedBox()
          : level == '2'
              ? FloatingActionButton.extended(
                  icon: Icon(Icons.check_circle),
                  label: Text('Admin Access'),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => ParaPembeli()));
                  },
                )
              : SizedBox(),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }

  Future<bool> _exitPopUp() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(16.0),
                      color: Colors.black,
                      child: Text(
                        'Konfirmasi',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(16.0),
                      child: Text('Kamu yakin ingin sign out?'),
                    ),
                    Container(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: SizedBox(),
                          ),
                          FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop(false);
                            },
                            child: Text('No'),
                          ),
                          FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop(true);
                              widget.keluar();
                              setState(() {
                                Navigator.of(context).pushReplacement(
                                    MaterialPageRoute(
                                        builder: (context) => LoginPage()));
                              });
                            },
                            child: Text('Yes'),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ) ??
              false;
        });
  }

  Future<bool> _hapusSemuaPesanan() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(16.0),
                      color: Colors.black,
                      child: Text(
                        'Konfirmasi',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(16.0),
                      child: Text(
                          'Apakah ingin menghapus pesanan kamu yang sebelumnya?'),
                    ),
                    Container(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: SizedBox(),
                          ),
                          FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop(false);
                            },
                            child: Text('No'),
                          ),
                          FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop(true);
                              setState(() {
                                _submitHapus();
                              });
                            },
                            child: Text('Yes'),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ) ??
              false;
        });
  }

  _statusOffline() {
    return showDialog(
        context: context,
        builder: (context) {
          return Dialog(
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(16.0),
                  color: Colors.black,
                  child: Text(
                    'Warning',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(16.0),
                  child: Text(
                      'Kamu sedang offline, periksa koneksi kamu dan coba kembali.'),
                ),
                Container(
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: SizedBox(),
                      ),
                      FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text('OK'),
                      )
                    ],
                  ),
                )
              ],
            ),
          );
        });
  }
}
