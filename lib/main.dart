import 'package:flutter/material.dart';
import 'package:ketixclone/view/login/login.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'eCommerce Late',
    theme: ThemeData(
      fontFamily: 'RaleWay',
    ),
    home: LoginPage(),
  ));
}
